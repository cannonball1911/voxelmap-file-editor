﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;

/* Solution: VoxelMap File Editor, Main.cs
 * Change the waypoint order and the names of the waypoints of a minecraft voxelmap file
 * Kai Sackl, 21.09.2020
 */

namespace VoxelMap_File_Editor
{
    public partial class Main : Form
    {
        #region Globals
        private string pointsFilePath;
        private ObservableCollection<string> waypoints;
        private enum Direction
        {
            MoveItemUp = -1,
            MoveItemDown = 1
        }

        private readonly string version = "v1.0.0";
        #endregion

        public Main()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            Text = $"VoxelMap File Editor {version}";

            pointsFilePath = "";
            waypoints = null;
        }

        #region Button Events
        private void BtnOpenFile_Click(object sender, EventArgs e)
        {
            Clear();

            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.InitialDirectory = @"C:\Users\Kai\AppData\Roaming\.minecraft\mods\VoxelMods\voxelMap";
                ofd.Filter = "points files (*.points)|*.points";

                if (ofd.ShowDialog() == DialogResult.OK)
                    pointsFilePath = ofd.FileName;
            }

            if (!string.IsNullOrEmpty(pointsFilePath))
            {
                waypoints = new ObservableCollection<string>(File.ReadLines(pointsFilePath));
                LbxWaypoints.Items.AddRange(waypoints.ToArray());
            }
        }

        private void BtnChangeName_Click(object sender, EventArgs e)
        {
            if (LbxWaypoints.SelectedItem == null)
                return;

            // Split row
            string[] splittedSelectedRow = ((string)LbxWaypoints.SelectedItem).Split(',');
            string[] splittedNameObject = splittedSelectedRow[0].Split(':');

            // Change name in the splitted selected row to name from textbox
            splittedNameObject[1] = TbxName.Text;
            splittedSelectedRow[0] = $"{splittedNameObject[0]}:{splittedNameObject[1]}";

            // Generate new row string
            string newRowWithLastComma = "";
            foreach (string splittedItem in splittedSelectedRow)
                newRowWithLastComma += $"{splittedItem},";
            string newRowWithoutLastComma = newRowWithLastComma.Remove(newRowWithLastComma.Length - 1);

            // Add new row string to listbox
            int lastSelectedIndex = LbxWaypoints.SelectedIndex;
            LbxWaypoints.Items.Remove(LbxWaypoints.SelectedItem);
            LbxWaypoints.Items.Insert(lastSelectedIndex, newRowWithoutLastComma);

            LbxWaypoints.SetSelected(lastSelectedIndex, true);
        }

        private void BtnSaveFile_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(pointsFilePath))
                File.WriteAllLines(pointsFilePath, LbxWaypoints.Items.OfType<string>().ToArray());

            Clear();
        }

        private void BtnUp_Click(object sender, EventArgs e)
        {
            MoveItem((int)Direction.MoveItemUp);
        }

        private void BtnDown_Click(object sender, EventArgs e)
        {
            MoveItem((int)Direction.MoveItemDown);
        }
        #endregion

        #region Helper Methods
        // Moves the selected item in the listbox and waypoints list in the dessired direction (up/down)
        private void MoveItem(int direction)
        {
            if (LbxWaypoints.SelectedItem == null || LbxWaypoints.SelectedIndex < 0)
                return;

            int oldIndex = LbxWaypoints.SelectedIndex;
            int newIndex = LbxWaypoints.SelectedIndex + direction;

            if (newIndex < 0 || newIndex >= LbxWaypoints.Items.Count)
                return;

            // "Move" item in listbox
            object selectedItem = LbxWaypoints.SelectedItem;
            LbxWaypoints.Items.Remove(selectedItem);
            LbxWaypoints.Items.Insert(newIndex, selectedItem);
            LbxWaypoints.SetSelected(newIndex, true);
            
            // Move item in waypoints list
            waypoints.Move(oldIndex, newIndex);
        }

        // Clears listbox, pointsFilePath and waypoints list
        private void Clear()
        {
            LbxWaypoints.Items.Clear();
            pointsFilePath = "";
            TbxName.Text = "";
            waypoints = null;
        }
        #endregion

        #region ListBox Events
        private void LbxWaypoints_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (LbxWaypoints.SelectedItem == null)
                return;
            string[] splittedSelectedString = ((string)LbxWaypoints.SelectedItem).Split(',');
            string[] splittedNameObject = splittedSelectedString[0].Split(':');
            TbxName.Text = splittedNameObject[1];
        }
        #endregion
    }
}
