﻿namespace VoxelMap_File_Editor
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LbxWaypoints = new System.Windows.Forms.ListBox();
            this.BtnOpenFile = new System.Windows.Forms.Button();
            this.BtnUp = new System.Windows.Forms.Button();
            this.BtnDown = new System.Windows.Forms.Button();
            this.BtnSaveFile = new System.Windows.Forms.Button();
            this.TbxName = new System.Windows.Forms.TextBox();
            this.LblName = new System.Windows.Forms.Label();
            this.BtnChangeName = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LbxWaypoints
            // 
            this.LbxWaypoints.FormattingEnabled = true;
            this.LbxWaypoints.Location = new System.Drawing.Point(12, 92);
            this.LbxWaypoints.Name = "LbxWaypoints";
            this.LbxWaypoints.Size = new System.Drawing.Size(696, 303);
            this.LbxWaypoints.TabIndex = 0;
            this.LbxWaypoints.SelectedIndexChanged += new System.EventHandler(this.LbxWaypoints_SelectedIndexChanged);
            // 
            // BtnOpenFile
            // 
            this.BtnOpenFile.Location = new System.Drawing.Point(276, 12);
            this.BtnOpenFile.Name = "BtnOpenFile";
            this.BtnOpenFile.Size = new System.Drawing.Size(214, 32);
            this.BtnOpenFile.TabIndex = 1;
            this.BtnOpenFile.Text = "Open file";
            this.BtnOpenFile.UseVisualStyleBackColor = true;
            this.BtnOpenFile.Click += new System.EventHandler(this.BtnOpenFile_Click);
            // 
            // BtnUp
            // 
            this.BtnUp.Location = new System.Drawing.Point(714, 92);
            this.BtnUp.Name = "BtnUp";
            this.BtnUp.Size = new System.Drawing.Size(74, 36);
            this.BtnUp.TabIndex = 2;
            this.BtnUp.Text = "Up";
            this.BtnUp.UseVisualStyleBackColor = true;
            this.BtnUp.Click += new System.EventHandler(this.BtnUp_Click);
            // 
            // BtnDown
            // 
            this.BtnDown.Location = new System.Drawing.Point(714, 134);
            this.BtnDown.Name = "BtnDown";
            this.BtnDown.Size = new System.Drawing.Size(74, 36);
            this.BtnDown.TabIndex = 3;
            this.BtnDown.Text = "Down";
            this.BtnDown.UseVisualStyleBackColor = true;
            this.BtnDown.Click += new System.EventHandler(this.BtnDown_Click);
            // 
            // BtnSaveFile
            // 
            this.BtnSaveFile.Location = new System.Drawing.Point(276, 406);
            this.BtnSaveFile.Name = "BtnSaveFile";
            this.BtnSaveFile.Size = new System.Drawing.Size(214, 32);
            this.BtnSaveFile.TabIndex = 4;
            this.BtnSaveFile.Text = "Save file";
            this.BtnSaveFile.UseVisualStyleBackColor = true;
            this.BtnSaveFile.Click += new System.EventHandler(this.BtnSaveFile_Click);
            // 
            // TbxName
            // 
            this.TbxName.Location = new System.Drawing.Point(56, 62);
            this.TbxName.Name = "TbxName";
            this.TbxName.Size = new System.Drawing.Size(495, 20);
            this.TbxName.TabIndex = 5;
            // 
            // LblName
            // 
            this.LblName.AutoSize = true;
            this.LblName.Location = new System.Drawing.Point(12, 65);
            this.LblName.Name = "LblName";
            this.LblName.Size = new System.Drawing.Size(38, 13);
            this.LblName.TabIndex = 6;
            this.LblName.Text = "Name:";
            // 
            // BtnChangeName
            // 
            this.BtnChangeName.Location = new System.Drawing.Point(557, 60);
            this.BtnChangeName.Name = "BtnChangeName";
            this.BtnChangeName.Size = new System.Drawing.Size(151, 24);
            this.BtnChangeName.TabIndex = 7;
            this.BtnChangeName.Text = "Change name";
            this.BtnChangeName.UseVisualStyleBackColor = true;
            this.BtnChangeName.Click += new System.EventHandler(this.BtnChangeName_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.BtnChangeName);
            this.Controls.Add(this.LblName);
            this.Controls.Add(this.TbxName);
            this.Controls.Add(this.BtnSaveFile);
            this.Controls.Add(this.BtnDown);
            this.Controls.Add(this.BtnUp);
            this.Controls.Add(this.BtnOpenFile);
            this.Controls.Add(this.LbxWaypoints);
            this.Name = "Main";
            this.Text = "VoxelMap File Editor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox LbxWaypoints;
        private System.Windows.Forms.Button BtnOpenFile;
        private System.Windows.Forms.Button BtnUp;
        private System.Windows.Forms.Button BtnDown;
        private System.Windows.Forms.Button BtnSaveFile;
        private System.Windows.Forms.TextBox TbxName;
        private System.Windows.Forms.Label LblName;
        private System.Windows.Forms.Button BtnChangeName;
    }
}

